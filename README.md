# RhoMemory (work is in progress)

A single-header C library that implements dynamic memory allocators.

## Getting started

### Add library to the project

 Use CMake and FetchContent. Fetch the library:

  ```cmake
  include(FetchContent)
  FetchContent_Declare(RhoMemory
    GIT_REPOSITORY https://gitlab.com/manenko/rho-memory.git
    GIT_TAG        v0.0.1)
  FetchContent_MakeAvailable(RhoMemory)
  ```

  Then link your CMake target with the library:

  ```cmake
  target_link_libraries(${PROJECT_NAME} PRIVATE RhoMemory)
  ```

### Optional configuration

You can define the following macros before including the header to change the
library's behavior:

## Example

```c
#include <rho/memory.h>

int main( void )
{
    return 0;
}
```

## License

MIT
