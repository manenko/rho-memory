/* rho/memory.h - v0.1.0 - MIT - Oleksandr Manenko, 2021
 * A single-header C library that implements a few dynamic memory allocators.
 *
 *******************************************************************************
 * USAGE
 *
 * This file provides both the interface and implementation.
 *
 * Copy the rho/memory.h into your source tree and then in exactly one source
 * file:
 *     #define RHO_MEMORY_IMPLEMENTATION
 *     #include "rho/memory.h"
 * This will create the implementation.
 * Other source files should just include "rho/memory.h".
 *
 * #include <rho/memory.h>
 *
 * int main( void ) {
 *     return 0;
 * }
 *
 *******************************************************************************
 * CONFIGURATION
 *
 * You can define the following macros before including the header to change the
 * library's behavior:
 *
 *******************************************************************************
 * LICENSE
 *
 * Scroll to the end of the file for license information.
 */

#ifndef RHO_MEMORY_H_INCLUDED
#    define RHO_MEMORY_H_INCLUDED ( 1 )

#    include <stddef.h>

#    ifdef __cplusplus
extern "C" {
#    endif

/* Interface ******************************************************************/

#    ifndef RHO_MEMORY_API_DECL
#        ifdef RHO_API_DECL
#            define RHO_MEMORY_API_DECL RHO_API_DECL
#        else
#            define RHO_MEMORY_API_DECL extern
#        endif
#    endif

/* A prototype  for a  function callback  that allocates  memory of  a requested
 * size.
 *
 * When implementing  this function, allocate a  block of at least  'size' bytes
 * and return a pointer to the start of the block.
 *
 * The 'size'  parameter must always  be greater  than 0. If  it is not,  or the
 * block cannot be allocated, return NULL.
 *
 * The 'context' parameter  is an untyped pointer to  an allocator-defined data.
 * It could be a NULL pointer. */
typedef void *( *rho_memory_alloc_cb_t )( size_t size, void *context );

/* A prototype for a function callback that deallocates a given block of memory.
 *
 * When implementing  this function, make the  block available for reuse  by the
 * allocator.
 *
 * The 'memory_block' parameter must point to  the memory block allocated by the
 * same allocator, otherwise the result is undefined. Do nothing, if it is NULL.
 *
 * The 'context' parameter  is an untyped pointer to  an allocator-defined data.
 * It could be a NULL pointer. */
typedef void ( *rho_memory_free_cb_t )( void *memory_block, void *context );

/* A prototype  for a  function callback  that allocates  memory of  a requested
 * size and alignment.
 *
 * When implementing  this function, allocate a  block of at least  'size' bytes
 * and return a pointer to the start of  the block. The block must be aligned as
 * specified by the 'alignment' parameter.
 *
 * The  'alignment'  parameter  must  be  a valid  alignment  supported  by  the
 * implementation. Return NULL if it is not.
 *
 * The  'size' parameter  must  always be  greater  than 0  and  be an  integral
 * multiple of  'alignment'. If  it is  not, or the  block cannot  be allocated,
 * return NULL.
 *
 * The 'context' parameter  is an untyped pointer to  an allocator-defined data.
 * It could be a NULL pointer. */
typedef void *( *rho_memory_aligned_alloc_cb_t )(
    size_t alignment,
    size_t size,
    void * context );

/* A prototype  for a function callback  that reallocates memory of  a requested
 * size for an existing memory block.
 *
 * When  implementing this  function, change  the size  of the  block of  memory
 * pointed to by  'memory_block' to the size specified by  'size' and return the
 * pointer  to the  larger  block of  memory. Return  NULL  on any  reallocation
 * failure, leaving the old block of memory untouched.
 *
 * The 'memory_block' parameter must point to  the memory block allocated by the
 * same allocator, otherwise the result is  undefined. If it is NULL, allocate a
 * block of  at least  'size' bytes  and return a  pointer to  the start  of the
 * block.
 *
 * The 'size'  parameter must  always be greater  than 0. If  it is  not, return
 * NULL.
 *
 * The 'context' parameter  is an untyped pointer to  an allocator-defined data.
 * It could be a NULL pointer. */
typedef void *( *rho_memory_realloc_cb_t )(
    void * memory_block,
    size_t size,
    void * context );

typedef void *( *rho_memory_context_free_cb_t )( void *context );

typedef struct rho_memory_allocator_t rho_memory_allocator_t;

typedef struct {
    rho_memory_alloc_cb_t         alloc;
    rho_memory_aligned_alloc_cb_t aligned_alloc;
    rho_memory_realloc_cb_t       realloc;
    rho_memory_free_cb_t          free;
} rho_memory_allocator_desc_t;

RHO_MEMORY_API_DECL rho_memory_allocator_t *rho_memory_allocator_alloc(
    rho_memory_allocator_desc_t *desc,
    void *                       context,
    rho_memory_allocator_t *     allocator );

RHO_MEMORY_API_DECL void rho_memory_allocator_free(
    rho_memory_allocator_t *     allocator,
    rho_memory_context_free_cb_t context_free );

RHO_MEMORY_API_DECL void *
rho_memory_alloc( rho_memory_allocator_t *allocator, size_t size );

RHO_MEMORY_API_DECL void *rho_memory_aligned_alloc(
    rho_memory_allocator_t *allocator,
    size_t                  alignment,
    size_t                  size );

RHO_MEMORY_API_DECL void *rho_memory_realloc(
    rho_memory_allocator_t *allocator,
    void *                  memory_block,
    size_t                  size );

RHO_MEMORY_API_DECL void
rho_memory_free( rho_memory_allocator_t *allocator, void *memory_block );

/* Return  a dynamic  memory  allocator  that uses  malloc,  realloc, free,  and
 * aligned_alloc functions from the standard library. */
RHO_MEMORY_API_DECL rho_memory_allocator_t *rho_memory_allocator_std( void );

/* Return a dynamic memory allocator that does nothing. */
RHO_MEMORY_API_DECL rho_memory_allocator_t *rho_memory_allocator_null( void );

#    ifdef __cplusplus
}
#    endif

#endif /* RHO_MEMORY_H_INCLUDED */

#ifdef RHO_MEMORY_IMPLEMENTATION
#    ifdef __cplusplus
extern "C" {
#    endif

/* Implementation *************************************************************/

#    ifndef RHO_MEMORY_API_IMPL
#        ifdef RHO_API_IMPL
#            define RHO_MEMORY_API_IMPL RHO_API_IMPL
#        else
#            define RHO_MEMORY_API_IMPL
#        endif
#    endif

#    ifndef RHO_MEMORY_UNUSED
#        ifdef RHO_UNUSED
#            define RHO_MEMORY_UNUSED( XXX ) RHO_UNUSED( XXX )
#        else
#            define RHO_MEMORY_UNUSED( XXX ) ( ( void )( XXX ) )
#        endif
#    endif

#    ifndef RHO_MEMORY_ASSERT
#        ifdef RHO_ASSERT
#            define RHO_MEMORY_ASSERT( COND ) RHO_ASSERT( COND )
#        else
#            include <assert.h>
#            define RHO_MEMORY_ASSERT( COND ) assert( COND )
#        endif
#    endif

#    ifndef RHO_MEMORY_MALLOC
#        ifdef RHO_MALLOC
#            define RHO_MEMORY_MALLOC( SIZE ) RHO_MALLOC( SIZE )
#        else
#            include <stdlib.h>
#            define RHO_MEMORY_MALLOC( SIZE ) malloc( SIZE )
#        endif
#    endif

#    ifndef RHO_MEMORY_REALLOC
#        ifdef RHO_REALLOC
#            define RHO_MEMORY_REALLOC( PTR, NEW_SIZE )                        \
                RHO_REALLOC( PTR, NEW_SIZE )
#        else
#            include <stdlib.h>
#            define RHO_MEMORY_REALLOC( PTR, NEW_SIZE ) realloc( PTR, NEW_SIZE )
#        endif
#    endif

#    ifndef RHO_MEMORY_FREE
#        ifdef RHO_FREE
#            define RHO_MEMORY_FREE( PTR ) RHO_FREE( PTR )
#        else
#            include <stdlib.h>
#            define RHO_MEMORY_FREE( PTR ) free( PTR )
#        endif
#    endif

#    ifndef RHO_MEMORY_ALIGNED_ALLOC
#        ifdef RHO_ALIGNED_ALLOC
#            define RHO_MEMORY_ALIGNED_ALLOC( ALIGNMENT, SIZE )                \
                RHO_ALIGNED_ALLOC( ALIGNMENT, SIZE )
#        else
#            include <stdlib.h>
#            define RHO_MEMORY_ALIGNED_ALLOC( ALIGNMENT, SIZE )                \
                aligned_alloc( ALIGNMENT, SIZE )
#        endif
#    endif

struct rho_memory_allocator_t {
    rho_memory_alloc_cb_t         alloc;
    rho_memory_aligned_alloc_cb_t aligned_alloc;
    rho_memory_realloc_cb_t       realloc;
    rho_memory_free_cb_t          free;
    void *                        context;
    rho_memory_allocator_t *      allocator;
};

RHO_MEMORY_API_IMPL rho_memory_allocator_t *rho_memory_allocator_alloc(
    rho_memory_allocator_desc_t *desc,
    void *                       context,
    rho_memory_allocator_t *     allocator )
{
    RHO_MEMORY_ASSERT( desc );

    if ( !desc ) {
        return NULL;
    }

    rho_memory_allocator_t *this = NULL;
    if ( allocator ) {
        this = rho_memory_alloc( allocator, sizeof( *this ) );
    } else if ( desc->alloc ) {
        this = desc->alloc( sizeof( *this ), context );
    }

    if ( this ) {
        this->alloc         = desc->alloc;
        this->aligned_alloc = desc->aligned_alloc;
        this->realloc       = desc->realloc;
        this->free          = desc->free;
        this->context       = context;
        this->allocator     = allocator;
    }

    return this;
}

RHO_MEMORY_API_IMPL void rho_memory_allocator_free(
    rho_memory_allocator_t *     allocator,
    rho_memory_context_free_cb_t context_free )
{
    RHO_MEMORY_ASSERT( allocator );

    if ( allocator ) {
        void *context = allocator->context;

        if ( allocator->allocator ) {
            rho_memory_free( allocator, allocator );
        } else if ( allocator->free ) {
            allocator->free( allocator, allocator->context );
        }

        if ( context_free ) {
            context_free( context );
        }
    }
}

RHO_MEMORY_API_IMPL void *
rho_memory_alloc( rho_memory_allocator_t *allocator, size_t size )
{
    RHO_MEMORY_ASSERT( allocator );

    if ( allocator && allocator->alloc ) {
        return allocator->alloc( size, allocator->context );
    }

    return NULL;
}

RHO_MEMORY_API_IMPL void *rho_memory_aligned_alloc(
    rho_memory_allocator_t *allocator,
    size_t                  alignment,
    size_t                  size )
{
    RHO_MEMORY_ASSERT( allocator );

    return allocator && allocator->aligned_alloc
             ? allocator->aligned_alloc( alignment, size, allocator->context )
             : NULL;
}

RHO_MEMORY_API_IMPL void *rho_memory_realloc(
    rho_memory_allocator_t *allocator,
    void *                  memory_block,
    size_t                  size )
{
    RHO_MEMORY_ASSERT( allocator );

    return allocator && allocator->realloc
             ? allocator->realloc( memory_block, size, allocator->context )
             : NULL;
}

RHO_MEMORY_API_IMPL void
rho_memory_free( rho_memory_allocator_t *allocator, void *memory_block )
{
    RHO_MEMORY_ASSERT( allocator );

    if ( allocator && allocator->free ) {
        allocator->free( memory_block, allocator->context );
    }
}

static void *rho_memory_std_alloc( size_t size, void *context )
{
    RHO_MEMORY_UNUSED( context );

    return size > 0 ? RHO_MEMORY_MALLOC( size ) : NULL;
}

static void *
rho_memory_std_aligned_alloc( size_t alignment, size_t size, void *context )
{
    RHO_MEMORY_UNUSED( context );

    /* TODO: What if a compiler does not support DR 460? [1]
     *       What if a compiler does not support N2072?  [2]
     *
     *       One option is to emulate this. Another one is to ignore it.
     *       Let's ignore it for the time being.
     *
     * [1]: http://www.open-std.org/jtc1/sc22/wg14/www/docs/summary.htm#dr_460
     * [2]: http://www.open-std.org/jtc1/sc22/wg14/www/docs/n2072.htm
     */
    return size > 0 ? RHO_MEMORY_ALIGNED_ALLOC( alignment, size ) : NULL;
}

static void *
rho_memory_std_realloc( void *memory_block, size_t size, void *context )
{
    RHO_MEMORY_UNUSED( context );

    /* We never reallocate std allocator because it is static */
    return memory_block != rho_memory_allocator_std() && size > 0
             ? RHO_MEMORY_REALLOC( memory_block, size )
             : NULL;
}

static void rho_memory_std_free( void *memory_block, void *context )
{
    RHO_MEMORY_UNUSED( context );

    /* We never free std allocator because it is static */
    if ( memory_block != rho_memory_allocator_std() && memory_block ) {
        RHO_MEMORY_FREE( memory_block );
    }
}

RHO_MEMORY_API_IMPL rho_memory_allocator_t *rho_memory_allocator_std( void )
{
    static rho_memory_allocator_t allocator = {
        .alloc         = rho_memory_std_alloc,
        .aligned_alloc = rho_memory_std_aligned_alloc,
        .realloc       = rho_memory_std_realloc,
        .free          = rho_memory_std_free,
        .context       = NULL,
        .allocator     = NULL,
    };

    return &allocator;
}

RHO_MEMORY_API_IMPL rho_memory_allocator_t *rho_memory_allocator_null( void )
{
    static rho_memory_allocator_t allocator = { 0 };

    return &allocator;
}

#    ifdef __cplusplus
}
#    endif

#endif /* RHO_MEMORY_IMPLEMENTATION */

/* Copyright 2021 Oleksandr Manenko
 *
 * Permission is hereby granted, free of  charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction,  including without limitation the rights
 * to use,  copy, modify,  merge, publish,  distribute, sublicense,  and/or sell
 * copies  of the  Software,  and to  permit  persons to  whom  the Software  is
 * furnished to do so, subject to the following conditions:
 *
 * The above  copyright notice and this  permission notice shall be  included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE  IS PROVIDED "AS IS",  WITHOUT WARRANTY OF ANY  KIND, EXPRESS OR
 * IMPLIED,  INCLUDING BUT  NOT LIMITED  TO THE  WARRANTIES OF  MERCHANTABILITY,
 * FITNESS FOR A  PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO  EVENT SHALL THE
 * AUTHORS  OR COPYRIGHT  HOLDERS  BE LIABLE  FOR ANY  CLAIM,  DAMAGES OR  OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
