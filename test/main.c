#define RHO_MEMORY_IMPLEMENTATION

#include <rho/memory.h>
#include <rho/test.h>

int main( void )
{
    rho_memory_allocator_t *std_alloc = rho_memory_allocator_std();

    void *ptr = rho_memory_alloc( std_alloc, 1024 );
    rho_test( "standard allocator allocates memory", ptr );

    ptr = rho_memory_realloc( std_alloc, ptr, 2048 );
    rho_test( "standard allocator reallocates memory", ptr );

    rho_memory_free( std_alloc, ptr );
    rho_memory_aligned_alloc(
        std_alloc,
        _Alignof( max_align_t ),
        100 * _Alignof( max_align_t ) );
    rho_test( "standard allocator allocates aligned memory", ptr );

    /* TODO: Check what functions return if the size is zero, ptr is NULL,
     *       alignment is not supported, etc.
     */

    rho_memory_free( std_alloc, ptr );

    /* TODO: Check null_allocator */
    /* TODO: Check allocator creation/destruction */

    return 0;
}

/* Copyright 2021 Oleksandr Manenko
 *
 * Permission is hereby granted, free of  charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction,  including without limitation the rights
 * to use,  copy, modify,  merge, publish,  distribute, sublicense,  and/or sell
 * copies  of the  Software,  and to  permit  persons to  whom  the Software  is
 * furnished to do so, subject to the following conditions:
 *
 * The above  copyright notice and this  permission notice shall be  included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE  IS PROVIDED "AS IS",  WITHOUT WARRANTY OF ANY  KIND, EXPRESS OR
 * IMPLIED,  INCLUDING BUT  NOT LIMITED  TO THE  WARRANTIES OF  MERCHANTABILITY,
 * FITNESS FOR A  PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO  EVENT SHALL THE
 * AUTHORS  OR COPYRIGHT  HOLDERS  BE LIABLE  FOR ANY  CLAIM,  DAMAGES OR  OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
